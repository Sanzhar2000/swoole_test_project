<?php


use Illuminate\Http\Request;
use SwooleTW\Http\Websocket\Facades\Websocket;
use SwooleTW\Http\Websocket\Middleware\Authenticate;
use Morilog\Jalali\Jalalian;

/*
|--------------------------------------------------------------------------
| Websocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register websocket events for your application.
|
*/

Websocket::on('connect', function () {
    echo "a client connected.\n";
    $user1 = \App\User::where('id', '=', 1)->first();
    Websocket::toUser([$user1])->emit('send', 'Hi there User #1');

    $user2 = \App\User::where('id', '=', 2)->first();
    Websocket::toUser([$user2])->emit('send', 'Hi there User #2');
})->middleware(Authenticate::class);

Websocket::on('disconnect', function ($websocket) {
    $user = auth()->user();
    echo "a " . $user . " client disconnected.\n";
});

Websocket::on('message', function ($websocket, $data) {
    echo $data . "\n";
    $websocket->broadcast()->emit('send', $data);
});
