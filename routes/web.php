<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use SwooleTW\Http\Websocket\Facades\Websocket;

use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', function (){
    $users = User::all();

    return view('users', compact('users'));
});

Route::get('test', function(){
    $userId = Websocket::getUserId();
    Websocket::toUserId($userId)->emit('message', 'hi there');
});

Route::get('users/{user}/chat', function (User $user) {
    return view('messenger', compact($user));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
