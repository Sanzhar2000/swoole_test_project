@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="card">
            <div class="card-header">Users</div>

            <div class="card-body">
                <ul>
                    @foreach($users as $user)
                        <a href="users/{{$user->id}}/chat"><li>{{ $user->name }} {{ $user->email }}</li></a>
                    @endforeach
                </ul>
            </div>
        </div>
        <div>

{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">Dashboard</div>--}}

{{--                    <div class="card-body">--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection
